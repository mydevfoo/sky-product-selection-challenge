## How to run the application ##

1. Run `python install -r requirements.txt` in the "server/" directory to install server dependencies.
2. Run `python app.py --host=0.0.0.0` in the "server/" directory to start the web server.
3. Open http://localhost:5000/ in your browser (I tested the app only in Chrome v54).
4. Set the 'customerID' cookie and refresh tha page. All available values are keys in server/sdks/fake/data/customers_info.json.

## Design considerations ##

1. The implementation does not uglify or minify the scripts. I would typically do this outside the development environment but it seems redundant to do it in the demo.
2. Flask serves index.html directly. In a real application, something like NGINX would do that. Doing it in the demo seemed like an overkill.
3. The "config/" folder would ideally be populated by something like Puppet. I checked it into the repository only for demonstration purposes.
4. It was not clear from the spec whether the challenge wanted a demonstration of how one would write a client that woul call services one-by-one. Therefore, I opted for the Gateway pattern, where a single server communicates with all the microservices, which is considered superior to direct client-to-microservice communication.

## How to run the tests ##

### Python ###

1. Run `python -m unittest discover` in the "server/" folder to run all Python tests.
2. Run `python -m unittest sdk.fake.tests.test_location_service` to run only the test defined in test_location_service.py

### Angular.JS ###

None have been added.