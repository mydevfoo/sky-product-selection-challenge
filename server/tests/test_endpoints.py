import unittest
import json
import app

class EndpointsTestCase(unittest.TestCase):
    def setUp(self):
        self.client = app.app.test_client()

    def test_get_available_products(self):
        # Request products available in Liverpool
        response  = self.client.get('/api/customers/5/products/available')
        resp_json = json.loads(response.data)
        expected  = [
            {'id': '3', 'category': 'Sports', 'name': 'Liverpool TV', 'locations': ['LIVERPOOL']},
            {'id': '4', 'category': 'News', 'name': 'Sky News', 'locations': ['NATIONWIDE']},
            {'id': '5', 'category': 'News', 'name': 'Sky Sports News', 'locations': ['NATIONWIDE']},
        ]

        self.assertItemsEqual(resp_json, expected)

        # Request products with a bad location_id
        response  = self.client.get('/api/customers/100/products/available')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 409)

    def test_confirm_selected_products(self):
        # Good request
        req_data  = json.dumps({'product_ids': ['3', '4']})
        response  = self.client.post('/api/customers/5/products/confirm_selected', data=req_data, content_type='application/json')
        resp_json = json.loads(response.data)
        expected  = {'confirmed_products': [
            {'id': '3', 'category': 'Sports', 'name': 'Liverpool TV', 'locations': ['LIVERPOOL']},
            {'id': '4', 'category': 'News', 'name': 'Sky News', 'locations': ['NATIONWIDE']},
        ]}

        self.assertItemsEqual(resp_json, expected)

        # Request with no body
        response  = self.client.post('/api/customers/5/products/confirm_selected')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 400)

        # Missing 'product_ids'
        req_data  = json.dumps({})
        response  = self.client.post('/api/customers/5/products/confirm_selected', data=req_data, content_type='application/json')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 400)

        # No products selected
        req_data  = json.dumps({'product_ids': []})
        response  = self.client.post('/api/customers/5/products/confirm_selected', data=req_data, content_type='application/json')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 400)

        # Bad location_id
        req_data  = json.dumps({'product_ids': ['1']})
        response  = self.client.post('/api/customers/100/products/confirm_selected', data=req_data, content_type='application/json')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 409)

        # Invalid product selected
        req_data  = json.dumps({'product_ids': ['100']})
        response  = self.client.post('/api/customers/5/products/confirm_selected', data=req_data, content_type='application/json')
        resp_json = json.loads(response.data)

        self.assertIn('message', resp_json['error'])
        self.assertEqual(response.status_code, 400)


if __name__ == '__main__':
    unittest.main()