import unittest
import helpers

class HelpersTestCase(unittest.TestCase):
    def test_json_error_response_body(self):
        """ Test that json_error_response_body returns body in the expected format. """

        messages = ['First test message', 'Second message']

        for message in messages:
            body = helpers.json_error_response_body(message)

            self.assertIn('error', body)
            self.assertIn('message', body['error'])
            self.assertEqual(body['error']['message'], message)


if __name__ == '__main__':
    unittest.main()