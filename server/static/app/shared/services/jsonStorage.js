'use strict';

productSelectionApp.service('JsonStorage', function() {
    this.set = function(key, value) {
        localStorage[key] = JSON.stringify(value);
    };

    this.get = function(key) {
        return JSON.parse(localStorage[key]);
    };

    this.delete = function(key) {
        delete localStorage[key];
    }
});