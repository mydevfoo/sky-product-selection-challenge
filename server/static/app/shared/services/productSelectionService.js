'use strict';

productSelectionApp.service('ProductSelectionService', ['$http', function($http) {
    this.getAvailableProducts = function(customerID) {
        return $http.get('/api/customers/' + customerID + '/products/available')
            .then(response => response.data);
    };

    this.getProductsByCategory = function(customerID) {
        return this.getAvailableProducts(customerID).then(function(availableProducts) {
            let productsByCategory = {};

            for (let product of availableProducts) {
                if (!(product.category in productsByCategory))
                    productsByCategory[product.category] = [];

                productsByCategory[product.category].push(product);
            }

            return productsByCategory;
        });
    };

    this.confirmSelectedProducts = function(customerID, selectedProductIDs) {
        return $http.post('/api/customers/' + customerID + '/products/confirm_selected', {product_ids: selectedProductIDs});
    };
}]);