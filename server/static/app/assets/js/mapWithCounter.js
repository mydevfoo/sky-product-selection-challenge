'use strict';

class MapWithCounter {
    // Associative array that can return the count of the elements it holds.
    //
    // Because it encapsulates the counting logic, it can be implemented in an efficient way, e.g. it can update the
    // counter inside set() and delete(). The current implementation does not do that because I wanted to keep the
    // code simple (more code equals a higher risk of bugs) and because premature optimisation is a bad practice.

    constructor(initialMap) {
        this._items = initialMap || {};
    }

    set(key, value) {
        this._items[key] = value;
    }

    get(key) {
        return this._items[key];
    }

    delete(key) {
        delete this._items[key];
    }

    count() {
        return Object.keys(this._items).length;
    }

    items() {
        return this._items;
    }
}