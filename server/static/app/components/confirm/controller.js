'use strict';

productSelectionApp.controller('ProductsConfirmationCtrl', ['$scope', 'JsonStorage', function($scope, JsonStorage) {
    let confirmationResponse = JsonStorage.get('confirmationResponse');

    if (confirmationResponse && 'confirmed_products' in confirmationResponse) {
        $scope.confirmedProducts = confirmationResponse.confirmed_products;
    } else {
        $scope.error = 'The product names are not available at the moment.';
    }
}]);