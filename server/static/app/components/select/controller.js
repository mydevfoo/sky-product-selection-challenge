'use strict';

productSelectionApp.controller('ProductSelectionCtrl', ['$scope', '$cookies', '$location', 'ProductSelectionService', 'JsonStorage',
    function($scope, $cookies, $location, ProductSelectionService, JsonStorage) {

    if (!('customerID' in $cookies)) {
        $scope.loadingStatus = 'Please set the "customerID" cookie and refresh the page.';
        return;
    }

    $scope.loadingStatus = 'Please wait while the data is loading...';

    ProductSelectionService.getProductsByCategory($cookies.customerID).then(function(productsByCategory) {
        $scope.productsByCategory = productsByCategory;
    }).catch(function(response) {
        try {
            $scope.loadingStatus = response.data.error.message;
        } catch(exc) {
            $scope.loadingStatus = 'Failed to load data. Please try again later.';
        }
    });

    // Using an associative array here has a drawback: Angular.JS will sort the items displayed in the basket. It may be
    // undesirable from the UX perspective, but it keeps the implementation simple which is probably better for the demo.
    $scope.selectedProducts = new MapWithCounter;

    $scope.updateProductSelection = function($event, product) {
        let checkbox = $event.target;

        if (checkbox.checked) {
            $scope.selectedProducts.set(product.id, product);
        } else {
            $scope.selectedProducts.delete(product.id);
        }
    };

    $scope.canCheckout = function() {
        return $scope.selectedProducts.count() > 0 && !$scope.submissionInProgress;
    };

    $scope.proceedToCheckout = function() {
        if (!$scope.selectedProducts.count())
            return;

        $scope.submissionInProgress = true;
        
        let productIDs = Object.keys($scope.selectedProducts.items());

        ProductSelectionService.confirmSelectedProducts($cookies.customerID, productIDs)
            .success(function(data) {
                JsonStorage.set('confirmationResponse', data);
                $location.path('/confirm');
            })
            .error(function(data) {
                if (data && data.error && data.error.message)
                    // Alert should not be used in production but will do for the demo :-)
                    alert(data.error.message);

                $scope.submissionInProgress = false;
            });
    };
}]);