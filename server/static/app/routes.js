'use strict';

productSelectionApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/select', {
            templateUrl: '/static/app/components/select/select.html',
            controller: 'ProductSelectionCtrl',
        })
       .when('/confirm', {
            templateUrl: '/static/app/components/confirm/confirm.html',
            controller: 'ProductsConfirmationCtrl'
        })
        .otherwise({
            redirectTo: '/select'
        });
}]);