from sdks.fake import load_data
from sdks.location_service import LocationNotAvailableError

class LocationServiceSDK(object):
    def __init__(self):
        self.customers_info = load_data('customers_info.json')

    def get_location_id(self, customer_id):
        try:
            customer_details = self.customers_info[customer_id]
        except KeyError:
            raise LocationNotAvailableError("There was a problem retrieving the customer information")

        return customer_details['location']