from sdks.fake import load_data

class CatalogueServiceSDK(object):
    def __init__(self):
        self.products_by_location = self._get_products_by_location()

    def _get_products_by_location(self):
        """
        Create an dictionary where keys are locations and values are product lists.
        Example: {'LONDON': [productA, productB], 'LEEDS': [productA]}
        """

        products_by_location = {}

        for product in load_data('products_info.json'):
            for location in product['locations']:
                if location not in products_by_location:
                    products_by_location[location] = []

                products_by_location[location].append(product)

        return products_by_location

    def get_available_products(self, location_id):
        return self.products_by_location['NATIONWIDE'] + self.products_by_location.get(location_id, [])