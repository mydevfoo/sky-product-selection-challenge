import os
import json

def load_data(filename):
    dir_path = os.path.realpath(os.path.dirname(__file__))
    filepath = os.path.join(dir_path, 'data/', filename)

    with open(filepath) as json_data:
        return json.load(json_data)
