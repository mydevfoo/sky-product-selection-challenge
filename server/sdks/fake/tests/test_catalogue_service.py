import unittest

from sdks.fake.catalogue_service import CatalogueServiceSDK

class CatalogueServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.location_service = CatalogueServiceSDK()

    def get_product_names_by_location_id(self, location_id):
        return [p['name'] for p in self.location_service.get_available_products(location_id)]

    def test_get_available_products(self):
        products = self.get_product_names_by_location_id(None)
        self.assertItemsEqual(products, ['Sky News', 'Sky Sports News'])

        products = self.get_product_names_by_location_id('LEEDS')
        self.assertItemsEqual(products, ['Sky News', 'Sky Sports News'])

        products = self.get_product_names_by_location_id('LIVERPOOL')
        self.assertItemsEqual(products, ['Sky News', 'Sky Sports News', 'Liverpool TV'])

        products = self.get_product_names_by_location_id('LONDON')
        self.assertItemsEqual(products, ['Sky News', 'Sky Sports News', 'Arsenal TV', 'Chelsea TV'])


if __name__ == '__main__':
    unittest.main()