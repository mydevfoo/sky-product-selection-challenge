import unittest

from sdks.location_service import LocationNotAvailableError
from sdks.fake.location_service import LocationServiceSDK

class LocationServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.location_service = LocationServiceSDK()

    def test_get_location_id(self):
        # Test that get_location_id() returns the correct ID for a valid location_id
        for customer_id, expected_location_id in (('1', 'LEEDS'), ('2', 'LONDON')):
            returned_location_id = self.location_service.get_location_id(customer_id)
            self.assertEqual(returned_location_id, expected_location_id)

        # Test that get_location_id() raises LocationNotAvailableError for non-existent customer_id
        self.assertRaises(LocationNotAvailableError, self.location_service.get_location_id, 'NON-EXISTENT-LOCATION-ID')


if __name__ == '__main__':
    unittest.main()