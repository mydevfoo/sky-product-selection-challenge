from flask import current_app

class InvalidSelectedProductError(ValueError):
    pass

class SelectedNothingError(ValueError):
    pass

class GatewayAPI(object):
    def __init__(self):
        self.location_service  = current_app.config['LOCATION_SERVICE']
        self.catalogue_service = current_app.config['CATALOGUE_SERVICE']

    def get_available_products(self, customer_id):
        location_id        = self.location_service.get_location_id(customer_id)
        available_products = self.catalogue_service.get_available_products(location_id)

        return available_products

    def validate_and_return_products(self, customer_id, selected_product_ids):
        """ Verify that the customer did not modify the request and the products he submitted are really available to him. """

        if not selected_product_ids:
            raise SelectedNothingError('Nothing was selected')

        available_products = self.get_available_products(customer_id)
        prod_id_to_product = {p['id']: p for p in available_products}

        result = []

        for prod_id in selected_product_ids:
            if prod_id not in prod_id_to_product:
                raise InvalidSelectedProductError('Invalid product found')

            result.append(prod_id_to_product[prod_id])

        return result