from flask import Blueprint, request, jsonify
from helpers import json_error_response_body
from blueprints.api.gateway_api import GatewayAPI, InvalidSelectedProductError, SelectedNothingError
from sdks.location_service import LocationNotAvailableError

api_blueprint = Blueprint('api_blueprint', __name__)

@api_blueprint.errorhandler(500)
def handle_uncaught_exceptions(err):
    return jsonify(json_error_response_body('Something went wrong'))

@api_blueprint.route('/customers/<customer_id>/products/available', methods=['GET'])
def get_available_products(customer_id):
    gateway_api = GatewayAPI()

    try:
        available_products = gateway_api.get_available_products(customer_id)
    except LocationNotAvailableError as exc:
        return jsonify(json_error_response_body(exc.message)), 409

    return jsonify(available_products)

@api_blueprint.route('/customers/<customer_id>/products/confirm_selected', methods=['POST'])
def confirm_selected_products(customer_id):
    gateway_api = GatewayAPI()

    try:
        received_product_ids = request.json['product_ids']
    except (KeyError, TypeError) as exc:
        return jsonify(json_error_response_body('Invalid data format')), 400

    try:
        products = gateway_api.validate_and_return_products(customer_id, received_product_ids)
    except LocationNotAvailableError as exc:
        return jsonify(json_error_response_body(exc.message)), 409
    except InvalidSelectedProductError as exc:
        return jsonify(json_error_response_body(exc.message)), 400
    except SelectedNothingError as exc:
        return jsonify(json_error_response_body(exc.message)), 400

    return jsonify({'confirmed_products': products})