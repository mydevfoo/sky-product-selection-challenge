import unittest
import app

from blueprints.api.gateway_api import GatewayAPI, InvalidSelectedProductError, SelectedNothingError
from sdks.fake.location_service import LocationServiceSDK
from sdks.location_service import LocationNotAvailableError

class GatewayAPITestCase(unittest.TestCase):
    def setUp(self):
        with app.app.app_context():
            self.gateway_api = GatewayAPI()

    def get_product_attributes(self, products, attribute_name):
        return [p[attribute_name] for p in products]

    def test_get_available_products(self):
        # Test that the method returns correct products for a valid customer_id
        products = self.gateway_api.get_available_products(customer_id='1')
        self.assertItemsEqual(self.get_product_attributes(products, 'name'), ['Sky News', 'Sky Sports News'])

        products = self.gateway_api.get_available_products(customer_id='2')
        self.assertItemsEqual(self.get_product_attributes(products, 'name'), ['Sky News', 'Sky Sports News', 'Arsenal TV', 'Chelsea TV'])

        # Test that invalid customer_id causes the method to raise InvalidSelectedProductError
        self.assertRaises(LocationNotAvailableError, self.gateway_api.get_available_products, 'NON-EXISTENT-LOCATION-ID')

    def test_validate_and_return_products(self):
        customer_id = '5'   

        # Confirm that location_id for this customer_id is what we think it is
        location_service = LocationServiceSDK()
        self.assertEqual(location_service.get_location_id(customer_id), 'LIVERPOOL')

        available_products    = self.gateway_api.get_available_products(customer_id)
        available_product_ids = self.get_product_attributes(available_products, 'id')

        # Selected all products available to the customer
        self.assertItemsEqual(available_products, self.gateway_api.validate_and_return_products(customer_id, available_product_ids))

        # Selected all products available to the customer except one
        removed_product_id    = available_product_ids.pop()
        available_products    = [p for p in available_products if p['id'] != removed_product_id]
        available_product_ids = self.get_product_attributes(available_products, 'id')

        self.assertItemsEqual(available_products, self.gateway_api.validate_and_return_products(customer_id, available_product_ids))

        # Selected a product not available to him
        london_products = self.gateway_api.get_available_products(customer_id='2')
        arsenal_tv      = next(p for p in self.gateway_api.get_available_products(customer_id='2') if p['name'] == 'Arsenal TV')

        self.assertIsNot(arsenal_tv, None)

        available_products.append(arsenal_tv)
        available_product_ids.append(arsenal_tv['id'])

        with self.assertRaises(InvalidSelectedProductError):
            self.gateway_api.validate_and_return_products(customer_id, available_product_ids)

        # Selected nothing at all
        with self.assertRaises(SelectedNothingError):
            self.gateway_api.validate_and_return_products(customer_id, [])


if __name__ == '__main__':
    unittest.main()