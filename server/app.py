from flask import Flask, send_file
from blueprints.api import api_blueprint

app = Flask(__name__)

app.config.from_object('config')
app.config.from_envvar('PRODUCT_SELECTION_APP_CONFIG_FILE', silent=True)

app.register_blueprint(api_blueprint, url_prefix='/api')

@app.route("/")
def index():
    # A recommended practice is to use NGINX rather than make Flask serve static files,
    # but to keep things simple for the demo purposes, serve the file directly.
    return send_file('static/index.html')

if __name__ == "__main__":
    app.run()