from sdks.fake.location_service import LocationServiceSDK
from sdks.fake.catalogue_service import CatalogueServiceSDK

LOCATION_SERVICE  = LocationServiceSDK()
CATALOGUE_SERVICE = CatalogueServiceSDK()